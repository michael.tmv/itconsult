# ITCONSULT

## ЗМІСТ

1. Створення найпростішої web-сторінки засобами HTML. 
2. Найпростіші способи оформлення тексту. 
3. CSS-властивості для оформлення тексту. Тег div.
4. Фон веб-сторінки. 
5. Створення списків.
6. Залік. Створення web-сторінки з текстом та її оформлення.
7. Розміщення зображень на сайті та їх оформлення. 
8. Створення власного сайту з зображеннями та текстом.
9. Розміщення відео та аудіо файлів на сайті. 
10. Внутрішні та зовнішні посилання, їх оформлення.
11. Блочна модель документу. Властивість display. Властивість box-sizing.
12. Селектори. Наслідування і каскадність в css. 
13. Створення таблиць за допомогою html-тегів table, tr, td.
14. Таблиці в css.
15. Залік. Створення сайту з таблицями.
16. Позиціонування. Властивість position. 
17. Меню. Горизонтальне та вертикальне меню. 
18. Плавні переходи. Властивість transition.
19. Трансформації. Властивість transform. 
20. Залік. Верстка сайту з заданим дизайном (6 год)
21. Форми.
22. Адаптивна та чуйна верстка (adaptive & responsive). Правило @media. 
23. Залік. Адаптивна верстка сайту із заданим дизайном (4 год)



# Pages
## Налаштування файлу **.gitlab-ci.yml**
> #### [GitLab Pages]( https://docs.gitlab.com/ee/user/project/pages/)
 
Мінімальні налаштування. Даний варіант розрахований на те, що всі необхідні файли знаходяться в папці `public/`
```
pages:
  stage: deploy
  script:  
    - echo "The site will be deployed to $CI_PAGES_URL"
  artifacts:  
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

```

### Файли можна скопіювати в папку `public`
```
pages:
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  ...

```


### Можна додати docker [image](https://hub.docker.com/)
```
image: busybox

pages:
  script:
    - echo "The site will be deployed to $CI_PAGES_URL"
  ...
```

---
[The Markdown Guide](https://www.markdownguide.org/)